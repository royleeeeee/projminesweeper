#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <signal.h>
#include <errno.h> 
#include <string.h> 
#include <netdb.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <signal.h>

#define FIELD_SPRITE ' '
#define FLAG_SPRITE 'F'
#define MAXCHARSIZE HEIGHT*WIDTH
#define HEIGHT 9
#define WIDTH 9
#define MAXLBSIZE LBHEIGHT*LBWIDTH
#define LBHEIGHT 10
#define LBWIDTH 4
#define RANDOM_NUMBER_SIZE 42

// Variable declaration
char username[100], 
	password[100],
	playField[HEIGHT][WIDTH],
	menuOption,
	minesweeperOption;	
int gameOver, 
	row, 
	col, 
	remainingBomb;
double epsilon = 0.001;

// Function declaration
void displayMainMenu();
void displayLeaderboard();
void startMinesweeper();
void initFields();
void placeMine();
int checkMine (int, int);
void printField();
void pickOptions(int *,int *, char *);
int playGame(char);
int revealTile(int, int);
int getAdjNum();
int placeFlag(int, int);
int verifyCoord(int,int);

// Server handling variable declaration
int clientFD, 
	numbytes;	
char *clientField,
	*clientLB,
	serverField[HEIGHT][WIDTH],
	serverLB[HEIGHT][WIDTH];
struct hostent *he; 
struct sockaddr_in their_addr;
	
// Server handling function declaration
void initSocket(int argc, char *argv[]);
int login();
void getField();
void sigHandler(int signo);
void sendChar(char *);
char *recvChar();
void sendInt(int);
int recvInt();

int main(int argc, char *argv[]) { 	
	initSocket(argc, argv);
	
	if(login()) {
		printf("\n  Accessing Minesweeper ...\n");
		displayMainMenu();
	} else {
		printf(
			"\n  Either username or password is incorrect. "
			"\n  Disconnecting from server...\n"
		);		
	}
	
	close(clientFD);	
	return 0;
}

// Display main menu
void displayMainMenu() {
	printf(
		"\n==========MAIN-MENU==========\n"
		"\n  Welcome to the Minesweeper gaming system.\n"
		"\n  Please enter a selection: \t"
		"\n  <1> Play Minesweeper \t"
		"\n  <2> Show Leaderboard \t"
		"\n  <3> Quit \n\t"
	);

	do {
		printf("\n  Selection option (1,2,3): ");
		scanf(" %c", &menuOption);		
	} while ((menuOption-48) != 1 
		&& (menuOption-48) != 2 
		&& (menuOption-48) != 3);
		
	if ((menuOption-48) == 1) {
		sendInt(1);
		startMinesweeper();
	} else if ((menuOption-48) == 2) {
		sendInt(2);
		displayLeaderboard();
	} else if ((menuOption-48) == 3) {
		sendInt(3);
		printf("\n  Exiting Program...\n");	
	} 
}

// Display leaderboard
void displayLeaderboard() {		
	printf("\n=========LEADERBOARD=========\n");		
	printf("\n  There is no information currently stored in the leaderboard.\n");	
	displayMainMenu();
}

// Start minesweeper game
void startMinesweeper() {		
	gameOver = 0;
	remainingBomb = recvInt();
	initFields();
	placeMine();		
    
	while (!gameOver) {
		getField();	
		
		pickOptions(&row, &col, &minesweeperOption);	
		gameOver = playGame(minesweeperOption);
		
		// Win condition
		if (!gameOver && remainingBomb == 0) {			
			int timeElapsed = recvInt();
			
			getField();
			printf(
				"\n  Congratulations! You have located all the mines. "
				"\n  You won in %d seconds.\n", timeElapsed
			);
			
			displayMainMenu();			
			
			gameOver = 1;		
		}
    } 
}

// Initialise the fields used by client
void initFields() {
    for(int row = 0; row < HEIGHT; row++) {
        for(int col = 0; col < WIDTH; col++) {			
            playField[row][col] = FIELD_SPRITE;
		}
	}		
}

// Send send random number to server to check if
// a tile have an existing mine
void placeMine() {
    int bombNum = recvInt(), i=0, col, row;
	srand(RANDOM_NUMBER_SIZE);
    for (i = 0; i < bombNum; i++) {
		do {
			col = rand() % (WIDTH);
			row = rand() % (HEIGHT);
		} while (checkMine(row, col));
    }
}

// Send coordinate to check for mine
int checkMine (int row, int col) {	
	sendInt(row);
	sendInt(col);
	
	int hasMine = recvInt();
	return hasMine;	
}

// Print out fields received from server in a 9x9 grid
void printField() {
	int row, col;
	
	printf(
		"\n=========MINESWEEPER=========\n"
		"\n  Remaining mines: %d\n\n  ", remainingBomb
	);	
	for(col = 0; col < (WIDTH*2)+7; col++) printf("-");		
	printf ("\n  |   | ");	 
    for(col = 0; col < WIDTH; col++) printf("%d ", col + 1);			
	printf("|\n  ");	
	for(col = 0; col < (WIDTH*2)+7; col++) printf("-");
	printf("\n");	 
    for(row = 0; row < HEIGHT; row++) {		
		printf("  | %c | ", 'A'+row);
        for(col = 0; col < WIDTH; col++) {			
            printf("%c ", serverField[row][col]);
        }
        printf("|\n");
    }	
	printf("  ");		
	for(col = 0; col < (WIDTH*2)+7; col++) printf("-");	
	printf("\n");	
}

// Let user select which minesweeper action to take
void pickOptions(int *row, int *col, char *minesweeperOption) {
	int rowInput, colInput;	
	char tileCoord[2], pickedOption;
	
	signal (SIGINT, sigHandler);	
		
	printf(
		"\n  Choose an option: \t"
		"\n  <R> Reveal Tile \t"
		"\n  <P> Place Flag \t"
		"\n  <Q> Quit Game \n\t"
	);

	do {
		printf("\n  Option (R,P,Q): ");
		scanf("%s", &pickedOption);		
	} while (
		tolower(pickedOption) != 'r' 
		&& tolower(pickedOption) != 'p' 
		&& tolower(pickedOption) != 'q'
	);
		
	*minesweeperOption = tolower(pickedOption);
	sendChar(minesweeperOption);
	
	// Send tile coordinate to server 
	if (tolower(pickedOption) != 'q') {		
		do {			
			printf("\n  Enter tile coordinates (ie. A2): ");
			scanf("%s", tileCoord);				
			rowInput = (tileCoord[0] - 96)-1;	
			colInput = (int) (tileCoord[1] - '0')-1;	
			*row = rowInput;
			*col = colInput;		
		} while (!verifyCoord(*row, *col) || playField[*row][*col] != FIELD_SPRITE);		
		sendInt(rowInput);
		sendInt(colInput);	
	}
}

// Get user selected options and execute corresponding functions
int playGame(char minesweeperOption) {		
	if (minesweeperOption == 'q') {		
		printf("\n  Exiting Minesweeper...\n");	
		displayMainMenu();
		return 1;
	} 
	else if (minesweeperOption == 'r') {	
		return revealTile(row, col);			
	} 
	else if (minesweeperOption == 'p') {	
		return placeFlag(row, col);
	}				
}

// When a coordinate is given, check for losing condition 
// and change tile sprite to adjacent mine number
int revealTile(int row, int col) {
	int i, j, adjMine;
	// Lose condition
	if (checkMine(row, col)) {			
		getField();
		printf("\n  Game over! You hit a mine.\n");
		displayMainMenu();
		return 1;
	}
	
	// Replace tile sprite with number of adjacent mines. 
	// Repeat for surrounding tiles if the number is zero
	else {
		adjMine = getAdjNum();
		playField[row][col] = (char)(adjMine+'0');
		
		if (adjMine == 0) {
			for (i = -1; i < 2; i++) {
				for (j = -1; j < 2; j++) {	  
					if (verifyCoord(row+i,col+j) 
						&& (playField[row+i][col+j] == FIELD_SPRITE)) {
						revealTile(row+i, col+j);
					}							
				}
			}
		}
	}
	return 0;
}

// Get adjacent mine number from server
int getAdjNum() {
	int adjMine = recvInt();
	return adjMine;
}

// Decrement the remaining bomb count if the given coordinate contains a mine
int placeFlag(int row, int col) {
	if (checkMine(row, col)) {
		remainingBomb--;
	} 
	return 0;		
}

// Check if given coordinate is out of bound
int verifyCoord(int row, int col) {
    if (row < 0 || row >= HEIGHT || col < 0 || col >= WIDTH) {
		return 0;
	} else {
		return 1;
	}
}

// Initialise socket
void initSocket(int argc, char *argv[]) {
	if (argc != 3) {
		fprintf(stderr,"usage: client_hostname port_number\n");
		exit(1);
	}

	if ((he=gethostbyname(argv[1])) == NULL) {
		herror("gethostbyname");
		exit(1);
	}

	if ((clientFD = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

	their_addr.sin_family = AF_INET;
	their_addr.sin_port = htons(atoi(argv[2])); 
	their_addr.sin_addr = *((struct in_addr *)he->h_addr);
	bzero(&(their_addr.sin_zero), 8); 

	if (connect(clientFD, (struct sockaddr *)&their_addr, sizeof(struct sockaddr)) == -1) {
		perror("connect");
		exit(1);
	}
}

// Send username and password to server and receive result
int login() {
	printf(
		"\n============LOGIN============\n"
		"\n  Login to access Minesweeper\n"
	);

	printf("\n  Username: ");
	scanf("%s", username);
	printf("\n  Password: ");
	scanf("%s", password);

	sendChar(username);
	sendChar(password);
	int loginSuccess = recvInt();
	
	return loginSuccess;
}

// Receive either minefield or playfield to display
void getField() {
	clientField = recvChar();	
	int cfIndex=0, sfRow=0, sfCol=0;
	while(cfIndex < MAXCHARSIZE) {					
		for (sfCol=0; sfCol < WIDTH; sfCol++) {					
			serverField[sfRow][sfCol] = clientField[cfIndex];
			cfIndex++;					
		}		
		sfRow ++;
	}
	
	printField();
}

// Handle interrupt signal (Ctrl + C)
void sigHandler(int signo) {
	sendChar("SIGINT");
	exit(1);
}

// Send char array to server
void sendChar(char *myArray) {
	int returnedBytes;
	uint16_t statistics;  
	for (int i = 0; i < MAXCHARSIZE; i++) {
		statistics = htons(myArray[i]);
		if ((returnedBytes=send(clientFD, &statistics, sizeof(uint16_t), 0)) == -1) {
			perror("send");
			exit(EXIT_FAILURE);
		}
	}
}

// Get char array from server
char *recvChar() {
	int returnedBytes;
    uint16_t statistics;
	
	char *results = malloc(sizeof(int)*MAXCHARSIZE);
	for (int i = 0; i < MAXCHARSIZE; i++) {
		if ((returnedBytes=recv(clientFD, &statistics, sizeof(uint16_t), 0)) == -1) {
			perror("recv");
			exit(EXIT_FAILURE);		    
		}
		results[i] = ntohs(statistics);
	}
	return results;
}

// Send single integer to server
void sendInt(int value) {
	int returnedBytes;
	uint32_t longVal = htonl(value);
	
	if ((returnedBytes=write(clientFD, &longVal, sizeof(uint32_t))) == -1) {
		perror("recv");
		exit(EXIT_FAILURE);		
	}	
}

// Get single integet from server
int recvInt(){
	int value, returnedBytes;
	
	if ((returnedBytes=read(clientFD, &value, sizeof(value))) == -1) {
		perror("recv");
		exit(EXIT_FAILURE);		
	}	
	
	uintptr_t row = (uintptr_t)	ntohl(value);
}








































































