#include <arpa/inet.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <errno.h> 
#include <string.h> 
#include <sys/types.h> 
#include <netinet/in.h> 
#include <sys/socket.h> 
#include <sys/wait.h> 
#include <unistd.h>
#include <errno.h>
#include <stdint.h>
#include <signal.h>
#include <time.h>

#define BOMB_SPRITE '+'
#define FIELD_SPRITE ' '
#define FLAG_SPRITE 'F'
#define MAXCHARSIZE HEIGHT*WIDTH 
#define HEIGHT 9
#define WIDTH 9
#define MAXLBSIZE LBHEIGHT*LBWIDTH
#define LBHEIGHT 10
#define LBWIDTH 4
#define RANDOM_NUMBER_SIZE 42
#define BACKLOG 10    
#define BOMB_NUM 10

// Variable declaration
int clientFD, 
	serverFD;
struct sockaddr_in my_addr;   
struct sockaddr_in their_addr;
socklen_t sin_size;

// Function Declaration
void initSocket(int argc, char *argv[]);
int authenticateLogin();
int checkLogin(char *, char *);
void serverProcess();
void sendChar(char *);
char *recvChar();
void sendInt(int);
int recvInt();

// Game handling variable declaration
int bombArray[BOMB_NUM][2],
	gameOver,
	remainingBomb,
	row,
	col;
char mineField[HEIGHT][WIDTH],
	playField[HEIGHT][WIDTH],
	*username,
	*password,
	*minesweeperOption;
double epsilon = 0.001,
	timeElapsed;
time_t timerStart = 0, 
	timerEnd = 0;

// Game handling function declaration
void getMainMenu();
void startMinesweeper();
void initFields();
void placeMine();
int checkMine (int, int);
int revealTile(int, int);
int getAdjNum(int,int,int[][2]);
int placeFlag(int, int);
void sendField(char fieldArray[HEIGHT][WIDTH]);
void printField(char fieldArray[][WIDTH]);
int verifyCoord(int,int);

int main(int argc, char *argv[]){
	initSocket(argc, argv);
	serverProcess();
}

// Initialise socket
void initSocket(int argc, char *argv[]) {
	if (argc != 2) {
		fprintf(stderr,"usage: client port_number\n");
		exit(1);
	}

	if ((clientFD = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
		perror("socket");
		exit(1);
	}

	my_addr.sin_family = AF_INET;        
	my_addr.sin_port = htons(atoi(argv[1])); 
	my_addr.sin_addr.s_addr = INADDR_ANY; 
	bzero(&(my_addr.sin_zero), 8);  

	if (bind(clientFD, (struct sockaddr *)&my_addr, sizeof(struct sockaddr)) == -1) {
		perror("bind");
		exit(1);
	}

	if (listen(clientFD, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}
	
	printf("server starts listnening ...\n");
}

// Handles the servide side of the game, checks for login and start the game
void serverProcess() {
	while(1) { 
		sin_size = sizeof(struct sockaddr_in);
		if ((serverFD = accept(clientFD, (struct sockaddr *)&their_addr, &sin_size)) == -1) {
			perror("accept");
			continue;
		}
		printf("\nserver: got connection from %s\n", inet_ntoa(their_addr.sin_addr));
		
		if (!fork()) { 			
			if (authenticateLogin()) {
				printf("Username: %s login successful\n", username);
				sendInt(1);				
				getMainMenu();
			} else {
				printf("Username: %s login failed\n", username);
				sendInt(0);				
			}			
			close(serverFD);
			exit(0);
		}
		
		close(serverFD); 
		while(waitpid(-1,NULL,WNOHANG) > 0); 
	}
}

// Get username and password from server and return result
int authenticateLogin() {
	username = recvChar();	
	password = recvChar();	
	
	return checkLogin(username, password);
	free(username);
	free(password);
}

// Receive username and password and check it against
// Authentication.txt. Return 1 if user and pw matches
int checkLogin(char *username, char *password) {
	char line[100], junk[100]; 
	int lineIndex=0, tempIndex=0, credentialStatus=0;

	FILE *file = fopen("Authentication.txt", "r");
	if(!file) {
		printf("Could not open file");
		return 0;
	}
	
	while(!feof(file)) {
		fscanf(file,"%[^ \n\t\r]s",line);
		
		if (lineIndex >= 2) { 
			if (lineIndex % 2 == 0) {
				if (strcmp(username, line) == 0) { 
					credentialStatus++;	
					tempIndex = lineIndex+1;	
				}
			} else if (lineIndex % 2 != 0 && credentialStatus == 1 && lineIndex == tempIndex) {
				if (strcmp(password, line) == 0) {
					credentialStatus++;
				}				
			}			 				
		}	
		lineIndex++;
		fscanf(file,"%[ \n\t\r]s",junk); 
	}
	fclose(file);
	
	if (credentialStatus==2) {
		return 1;
	} else {
		return 0;
	}
}
 
 // Get main menu option from client
void getMainMenu() {	
	int mainMenuOption = recvInt();
	if (mainMenuOption == 1) {
		printf("Starting Minesweeper\n");
		startMinesweeper();
	} 
	else if (mainMenuOption == 2) {		
		printf("Displaying Leaderboard\n");		
		getMainMenu();
	} 
	else if (mainMenuOption == 3) {
		printf("Quitting Program\n");
	} 
}

// Start minesweeper game
void startMinesweeper() {	
	// Start timer here
	time(&timerStart);	
	
	gameOver = 0;
	
	remainingBomb = BOMB_NUM;
	sendInt(remainingBomb);
	
	initFields();
	placeMine(mineField);		
    
	// Main game loop
	while (!gameOver) {		
		sendField(playField);
		printField(mineField);		
				
		minesweeperOption = recvChar();		
		
		// Exit game loop if sigint is received
		if (strcmp(minesweeperOption, "SIGINT") == 0) {	
			exit(1);
		}
		
		// Get coordinates from client and assign to row and col
		if (strcmp(minesweeperOption, "q") != 0){
			printf("Option: %s\n", minesweeperOption);
			row = recvInt();
			col = recvInt();
			printf("Row: %d\n", row);
			printf("Col: %d\n", col);
		} 
		
		if (strcmp(minesweeperOption, "q") == 0) {		
			printf("Exiting Minesweeper\n");	
			getMainMenu();
			gameOver = 1;
		} else if (strcmp(minesweeperOption, "r") == 0) {		
			gameOver = revealTile(row, col);		
		} else if (strcmp(minesweeperOption, "p") == 0) {	
			gameOver = placeFlag(row, col);		
		}				
				
		// Win condition
		if (!gameOver && remainingBomb == 0) {			
			// End timer here
			time(&timerEnd);  
			timeElapsed = difftime(timerEnd, timerStart);
			sendInt(timeElapsed);
			
			sendField(mineField);
			printField(mineField);	
			printf("\n%s won in %.0f seconds.\n", username, timeElapsed);
				
			getMainMenu();			
			gameOver = 1;		
		}
    } 
}

// Fill playField and mineField array with empty chars
void initFields() {
    int row, col;
	
    for(row = 0; row < HEIGHT; row++) {
        for(col = 0; col < WIDTH; col++) {		
            playField[row][col] = FIELD_SPRITE;		
            mineField[row][col] = FIELD_SPRITE;	
		}
	}		
}

// Randomly place mines into mineField and store coordinates in bombArray
void placeMine() {
	int i, col, row;
	srand(RANDOM_NUMBER_SIZE);
     
	sendInt(BOMB_NUM);
    for (i = 0; i < BOMB_NUM; i++) {
		do {
			col = rand() % (WIDTH);
			row = rand() % (HEIGHT);
			bombArray[i][0] = row;
			bombArray[i][1] = col;
		} while (checkMine(row, col));
		
		mineField[row][col] = BOMB_SPRITE;
    }
}

// Check if coordinate has existing mine
int checkMine (int row, int col) {
	row = recvInt();
	col = recvInt();
	if (mineField[row][col] == BOMB_SPRITE) {
		sendInt(1);
		return 1;
	} else {
		sendInt(0);
		return 0;
	}
}

// When a coordinate is given, check for losing condition 
// and change tile sprite to adjacent mine number
int revealTile(int row, int col) {
	int i, j, adjMine;
	
	// Lose condition
	if (checkMine(row, col)) {	
		// End timer here
		time(&timerEnd);  
		
		sendField(mineField);
		printField(mineField);
		printf("\nHit a mine, game over.\n");
		//displayMainMenu();
		getMainMenu();
		return 1;
	} 
	
	else {
		adjMine = getAdjNum(row, col, bombArray);
		playField[row][col] = (char)(adjMine+'0');
		mineField[row][col] = (char)(adjMine+'0');
		
		if (adjMine == 0) {
			for (i = -1; i < 2; i++) {
				for (j = -1; j < 2; j++) {	  
					if (verifyCoord(row+i,col+j) 
						&& (playField[row+i][col+j] == FIELD_SPRITE)) {
						revealTile(row+i, col+j);
					}							
				}
			}
		}
	}	
	return 0;
}

// Get number of adjacent mines
int getAdjNum(int row, int col, int bombArray[][2]) {
    int i, adjMine = 0;
	
    for (i = 0; i < BOMB_NUM; i++) {
        if ((abs(row-bombArray[i][0]) <= 1+epsilon) 
			&& (abs(col-bombArray[i][1]) <= 1+epsilon)) {
            adjMine++; 
        }  
    }
    
	sendInt(adjMine);
    return adjMine;
}

// Decrement the remaining bomb count if the given coordinate contains a mine
int placeFlag(int row, int col) {
	if (checkMine(row, col)) {
		playField[row][col] = BOMB_SPRITE;	
		mineField[row][col] = BOMB_SPRITE;
		remainingBomb--;
	} else {
		playField[row][col] = FLAG_SPRITE;	
		mineField[row][col] = FLAG_SPRITE;
	}
	
	return 0;		
}

// Send fieldArray to client side to display
void sendField(char fieldArray[HEIGHT][WIDTH]) {
	int sfIndex = 0;
	char serverField[MAXCHARSIZE];			
	while(sfIndex < MAXCHARSIZE) {
		for (int i = 0; i < HEIGHT; i++) {
			for (int j=0; j < WIDTH; j++) {
				serverField[sfIndex] = fieldArray[i][j];
				sfIndex++;
			}				
		}
	}		
	sendChar(serverField);
}

// Print out characters of an array in a 9x9 grid
void printField(char fieldArray[HEIGHT][WIDTH]) {
	int row, col;
	
	printf("\nRemaining mines: %d\n", remainingBomb);	
	for(col = 0; col < (WIDTH*2)+7; col++) printf("-");		
	printf ("\n|   | ");	 
    for(col = 0; col < WIDTH; col++) printf("%d ", col + 1);			
	printf("|\n");	
	for(col = 0; col < (WIDTH*2)+7; col++) printf("-");
	printf("\n");	 
    for(row = 0; row < HEIGHT; row++) {		
		printf("| %c | ", 'A'+row);
        for(col = 0; col < WIDTH; col++) {			
            printf("%c ", fieldArray[row][col]);
        }
        printf("|\n");
    }		
	for(col = 0; col < (WIDTH*2)+7; col++) printf("-");	
	printf("\n");	
}

// Check if given coordinate is out of bound
int verifyCoord(int row, int col) {
    if (row < 0 || row >= HEIGHT || col < 0 || col >= WIDTH) {
		return 0;
	} else {
		return 1;
	}
}

// Send char array to client
void sendChar(char *myArray) {
	int returnedBytes;
	uint16_t statistics;  
	for (int i = 0; i < MAXCHARSIZE; i++) {
		statistics = htons(myArray[i]);
		if ((returnedBytes=send(serverFD, &statistics, sizeof(uint16_t), 0)) == -1) {
			perror("send");
			exit(EXIT_FAILURE);
		}
	}
}

// Get char array from client
char *recvChar() {
	int returnedBytes;
    uint16_t statistics;
	
	char *results = malloc(sizeof(int)*MAXCHARSIZE);
	for (int i = 0; i < MAXCHARSIZE; i++) {
		if ((returnedBytes=recv(serverFD, &statistics, sizeof(uint16_t), 0)) == -1) {
			perror("recv");
			exit(EXIT_FAILURE);		    
		}
		results[i] = ntohs(statistics);
	}
	return results;
}

// Send single int to client
void sendInt(int value) {
	int returnedBytes;
	uint32_t longVal = htonl(value);
	
	if ((returnedBytes=write(serverFD, &longVal, sizeof(uint32_t))) == -1) {
		perror("recv");
		exit(EXIT_FAILURE);		
	}	
}

// Receive single int from client
int recvInt(){
	int value, returnedBytes;
	
	if ((returnedBytes=read(serverFD, &value, sizeof(value))) == -1) {
		perror("recv");
		exit(EXIT_FAILURE);		
	}	
	
	uintptr_t row = (uintptr_t)	ntohl(value);
}





















































